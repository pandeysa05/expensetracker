from django.urls import path
from .views import *

app_name = "expapp"

urlpatterns = [
    path("", HomeView.as_view()),
    path("expenses/", ExpenseListView.as_view()),
    path("nepal/", NepalView.as_view()),
    path("add-expense/", ExpenseEntryView.as_view()),
]